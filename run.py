# -*- coding: utf-8 -*-
# encoding=utf8
from Projekt import app, socketio

if __name__ == '__main__':
    app.debug = True
    socketio.run(app, debug=True)
