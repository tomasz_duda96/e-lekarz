from flask import Flask
from flask_mysqldb import MySQL
from flask_socketio import SocketIO


app = Flask(__name__)
app.config['SECRET_KEY'] = 'jsbcfsbfjefebw237u3gdbdc'
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = 'root'
app.config['MYSQL_DB'] = 'elekarzdb'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'
app.config['UPLOAD_FOLDER'] = 'Projekt/static/img'
mysql = MySQL(app)
socketio = SocketIO(app)

from Projekt import views, chat
