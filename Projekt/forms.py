from wtforms import Form, StringField, TextAreaField, PasswordField, validators


class RegisterFormP(Form):
    imie = StringField('Imie', [validators.Length(min=1, max=50, message="Pole musi zawierać od 1 do 50 znaków.")])
    nazwisko = StringField('Nazwisko', [validators.Length(min=1, max=50, message="Pole musi zawierać"
                                                                                 " od 1 do 50 znaków.")])
    email = StringField('E-mail', [validators.Email(message='Proszę poprawnie wprowadzić E-mail')])
    nrtelefonu = StringField('Numer telefonu', [validators.Length(min=8, message='Numer telefonu musi '
                                                                                 'zawierać od 8 do 15 cyfr')])
    pesel = StringField('PESEL', [validators.Length(min=11, message='PESEL musi zawierać 11 cyfr')])
    login = StringField('Login', [validators.Length(min=5, max=25, message="Login musi zawierać od 5 do 50 znaków")])
    haslo = PasswordField('Haslo', [validators.DataRequired(message="Proszę wprowadzić hasło"),
                                    validators.EqualTo('potwierdz', message='Hasla musza byc takie same')])
    potwierdz = PasswordField('Potwierdz haslo')


class RegisterFormL(Form):
    imie = StringField('Imie', [validators.Length(min=1, max=50, message="Pole musi zawierać od 1 do 50 znaków.")])
    nazwisko = StringField('Nazwisko', [validators.Length(min=1, max=50, message="Pole musi zawierać"
                                                                                 " od 1 do 50 znaków.")])
    email = StringField('E-mail', [validators.Email(message='Proszę poprawnie wprowadzić E-mail')])
    nrtelefonu = StringField('Numer telefonu', [validators.Length(min=8, message='Numer telefonu musi '
                                                                                 'zawierać od 8 do 15 cyfr')])
    pesel = StringField('PESEL', [validators.Length(min=11, message='PESEL musi zawierać 11 cyfr')])
    login = StringField('Login', [validators.Length(min=5, max=50, message="Login musi zawierać od 5 do 50 znaków")])
    haslo = PasswordField('Haslo', [validators.DataRequired(message="Proszę wprowadzić hasło"),
                                    validators.EqualTo('potwierdz', message='Hasla musza byc takie same')])
    potwierdz = PasswordField('Potwierdz haslo')
    specjalizacja = StringField('Specjalizacja', [validators.Length(min=1, max=50, message="Proszę wprowadzić"
                                                                                           " specjalizację")])
    nrPrawaWykonywaniaZawodu = StringField('Numer prawa wykonywania zawodu',
                                           [validators.Length(min=7, message="Numer wykonywania zawodu"
                                                                             " musi zawierać 7 cyfr")])


class SettingsForm(Form):
    email = StringField('E-mail', [validators.Email(message='Proszę poprawnie wprowadzić E-mail')])
    nrtelefonu = StringField('Numer telefonu', [validators.Length(min=8, message='Numer telefonu musi'
                                                                                 ' zawierać od 8 do 15 cyfr')])
    haslo = PasswordField('Haslo', [validators.DataRequired(message="Proszę wprowadzić hasło"),
                                    validators.EqualTo('potwierdz', message='Hasla musza byc takie same')])
    potwierdz = PasswordField('Potwierdz haslo')


class WiadomoscForm(Form):
    zawartosc = TextAreaField('Zawartość wiadomości', [validators.Length(min=1)])


class KonsultacjeForm(Form):
    typ_konsultacji = StringField('Rozpocznij konsultację jako:')
    temat = StringField('Temat konsultacji', [validators.Length(min=5)])


class DiagnozyForm(Form):
    diagnoza = TextAreaField('Diagnoza', [validators.Length(min=1)])