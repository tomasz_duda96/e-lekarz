from flask_socketio import join_room
from Projekt import socketio
from Projekt.utils import db_connection
from Projekt.sieci.test_model import check_image


@db_connection
def fetch_chat(cur, lekarz, pacjent, lekarz_porada=False):

    if lekarz_porada:
        cur.execute("SELECT * FROM lekarz_chat WHERE (OdbiorcaID = {} AND NadawcaID = {})"
                    " OR (NadawcaID = {} AND OdbiorcaID = {})".format(lekarz, pacjent, lekarz, pacjent))
        results = cur.fetchone()
    else:
        cur.execute("SELECT * FROM pacjent_chat WHERE (OdbiorcaID = {} AND NadawcaID = {})"
                    " OR (NadawcaID = {} AND OdbiorcaID = {})".format(lekarz, pacjent, lekarz, pacjent))
        results = cur.fetchone()
    return results


@socketio.on('private_message')
@db_connection
def private_message(cur, data):
    chat = data['chat_id']
    zawartosc = data['message']
    nadawca = data['user']

    if data['is_doc']:
        room = 'l' + chat
        cur.execute("INSERT INTO lekarz_wiadomosci(ChatID, zawartosc, nadawca) VALUES ('{}', '{}', '{}')".format(chat, zawartosc, nadawca))
    else:
        room = 'p' + chat
        cur.execute("INSERT INTO pacjent_wiadomosci(ChatID, zawartosc, nadawca) VALUES ('{}', '{}', '{}')".format(chat, zawartosc, nadawca))

    socketio.emit('private_response', data, room=room)


@socketio.on('connection')
@db_connection
def on_connect(cur, data):
    chat = data['chat_id']
    if data['is_doc']:
        room = 'l' + chat
        join_room(room)
    else:
        room = 'p' + chat
        join_room(room)


@socketio.on('analyze_picture')
def analyze(data):
    risk = check_image(data['source'][12:])
    if risk.item(0) == 1:
        data['border'] = 'green'
    else:
        data['border'] = 'red'

    socketio.emit('analysys_result', data, room='p2')
