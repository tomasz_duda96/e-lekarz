from functools import wraps
from flask import session, flash, redirect, url_for
from Projekt import mysql


def db_connection(func):
    @wraps(func)
    def wrap(*args, **kwargs):
        cur = mysql.connection.cursor()
        try:
            cur.execute("BEGIN")
            ret_function = func(cur, *args, **kwargs)
            cur.execute("COMMIT")
        except Exception:
            cur.execute("ROLLBACK")
            raise Exception
        finally:
            cur.close()
        return ret_function

    return wrap


def is_logged_in(user):
    def dec(func):
        @wraps(func)
        def wrap(*args, **kwargs):
            if 'logged_inL' not in session and 'logged_inP' not in session:
                flash('Brak dostępu, proszę się zalogować', 'danger')
                return redirect(url_for('zalogujsie'))
            elif user == 'user' and (session.get('logged_inL', False) or session.get('logged_inP', False)):
                return func(*args, **kwargs)
            elif (user == 'lekarz' and session.get('logged_inL', False)) or (user == 'pacjent' and session.get('logged_inP', False)):
                return func(*args, **kwargs)

        return wrap
    return dec
