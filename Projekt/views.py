import os
from flask import render_template, url_for, flash, redirect, session, request
from werkzeug.utils import secure_filename
from passlib.hash import sha256_crypt
from Projekt import app
from Projekt.utils import is_logged_in, db_connection
from Projekt.forms import RegisterFormP, RegisterFormL, SettingsForm
from Projekt.chat import fetch_chat


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/informacje')
def informacje():
    return render_template('informacje.html')


@app.route('/zalozkonto')
def zalozkonto():
    return render_template('zalozkonto.html')


@app.route('/pacjent')
@is_logged_in('pacjent')
def pacjent():
    return render_template('pacjent.html')


@app.route('/lekarz')
@is_logged_in('lekarz')
def lekarz():
    return render_template('lekarz.html')


@app.route('/wyloguj')
def wyloguj():
    session.clear()
    flash('Zastałeś poprawnie wylogowany', 'success')
    return redirect(url_for('index'))


@app.route('/twojeporady')
@is_logged_in('lekarz')
@db_connection
def twojeporady(cur):

    cur.execute("SELECT * FROM lekarz_chat WHERE lekarz_chat.NadawcaID = '{}'"
                " OR lekarz_chat.OdbiorcaID = '{}'".format(session['id'], session['id']))
    results = cur.fetchall()
    for result in results:
        if result['OdbiorcaID'] == session['id']:
            cur.execute("SELECT * FROM lekarz WHERE lekarz.id = '{}'".format(result['NadawcaID']))
            result.update(cur.fetchone())

        elif result['NadawcaID'] == session['id']:
            cur.execute("SELECT * FROM lekarz WHERE lekarz.id = '{}'".format(result['OdbiorcaID']))
            result.update(cur.fetchone())

    if results:
            return render_template('twojeporady.html', konsultacje=results, chat_type='lekarz')
    else:
        msg = 'Brak konsultacji'
        return render_template('twojekonwersacje.html', msg=msg)


@app.route('/twojekonwersacje')
@is_logged_in('user')
@db_connection
def twojekonwersacje(cur):

    if session.get('logged_inP', False):
        cur.execute("SELECT * FROM pacjent_chat JOIN lekarz ON pacjent_chat.OdbiorcaID = lekarz.id"
                    " WHERE pacjent_chat.NadawcaID = {}".format(session['id']))
    elif session.get('logged_inL', False):
        cur.execute("SELECT * FROM pacjent_chat JOIN pacjent ON pacjent_chat.NadawcaID = pacjent.id"
                    " WHERE pacjent_chat.OdbiorcaID = {}".format(session['id']))

    results = cur.fetchall()

    if results:
            return render_template('twojekonwersacje.html', konsultacje=results, chat_type='pacjent')
    else:
        msg = 'Brak konsultacji'
        return render_template('twojekonwersacje.html', msg=msg)


@app.route('/zalogujsie', methods=["GET", "POST"])
@db_connection
def zalogujsie(cur):
    if request.method == 'POST':
        login, haslo_kandydat = request.form['login'], request.form['haslo']

        if cur.execute("SELECT * FROM lekarz WHERE login = '{}' ".format(login)) > 0:
            data = cur.fetchone()
            if sha256_crypt.verify(haslo_kandydat, data['haslo']):
                session['logged_inL'] = True
                session.update(data)
                flash('Zostałeś poprawnie zalogowany', 'success')
                return redirect(url_for('lekarz'))
        elif cur.execute("SELECT * FROM pacjent WHERE login = '{}' ".format(login)) > 0:
            data = cur.fetchone()
            if sha256_crypt.verify(haslo_kandydat, data['haslo']):
                session['logged_inP'] = True
                session.update(data)
                flash('Zostałeś poprawnie zalogowany', 'success')
                return redirect(url_for('pacjent'))
        else:
            error = 'Dane niepoprawne'
            return render_template('zalogujsie.html', error=error)

    return render_template('zalogujsie.html')


@app.route('/ustawieniaprofilu', methods=["GET", "POST"])
@is_logged_in('user')
@db_connection
def ustawieniaprofilu(cur):

    form = SettingsForm(request.form)
    identyfikator = request.args.get('identyfikator')
    if request.method == "POST" and form.validate():
        email = form.email.data
        nrtelefonu = form.nrtelefonu.data
        haslo = sha256_crypt.encrypt(str(form.haslo.data))
        if session['logged_inL']:
            cur.execute("UPDATE lekarz SET email = %s, nr_telefonu = %s, haslo = %s WHERE id = %s",
                        (email, nrtelefonu, haslo, identyfikator))
        elif session['logged_inP']:
            cur.execute("UPDATE pacjent SET email = %s, nr_telefonu = %s, haslo = %s WHERE id = %s",
                        (email, nrtelefonu, haslo, identyfikator))
        else:
            flash('Brak dostępu, proszę się zalogować', 'danger')
            return redirect(url_for('zalogujsie'))

        flash('Zapisano nowe ustawienia', 'success')
        return redirect(url_for('lekarz'))
    return render_template('ustawieniaprofilu.html', form=form)


@app.route('/zalozkontoPacjent', methods=["GET", "POST"])
@db_connection
def zalozkontoP(cur):
    form = RegisterFormP(request.form)
    if request.method == "POST" and form.validate():
        imie = form.imie.data
        nazwisko = form.nazwisko.data
        email = form.email.data
        nrtelefonu = form.nrtelefonu.data
        pesel = form.pesel.data
        login = form.login.data
        haslo = sha256_crypt.encrypt(str(form.haslo.data))

        cur.execute("INSERT INTO pacjent(imie, nazwisko, email, nr_telefonu, pesel, login, haslo)"
                    " VALUES(%s, %s, %s, %s, %s, %s, %s)", (imie, nazwisko, email, nrtelefonu, pesel, login, haslo))

        flash('Konto zostało założone', 'success')
        redirect(url_for('zalogujsie'))
    return render_template('zalozkontoP.html', form=form)


@app.route('/zalozkontoLekarz', methods=['GET', "POST"])
@db_connection
def zalozkontoL(cur):
    form = RegisterFormL(request.form)
    if request.method == "POST" and form.validate():
        imie = form.imie.data
        nazwisko = form.nazwisko.data
        email = form.email.data
        nrtelefonu = form.nrtelefonu.data
        pesel = form.pesel.data
        specjalizacja = form.specjalizacja.data
        nrprawawykonywaniazawodu = form.nrPrawaWykonywaniaZawodu.data
        login = form.login.data
        haslo = sha256_crypt.encrypt(str(form.haslo.data))

        cur.execute("INSERT INTO "
                    "lekarz(imie, nazwisko, email, nr_telefonu, pesel, specjalizacja, nr_wykonywania_zawodu, login, haslo)"
                    " VALUES('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}')".format(
                     imie, nazwisko, email, nrtelefonu, pesel, specjalizacja, nrprawawykonywaniazawodu, login, haslo))

        flash('Konto zostało założone', 'success')
        redirect(url_for('zalogujsie'))
    return render_template('zalozkontoL.html', form=form)


@app.route('/listalekarzy')
@db_connection
def listalekarzy(cur):
    result = cur.execute("SELECT * FROM lekarz")
    danekontaktowe = cur.fetchall()

    if result > 0:
        return render_template('listalekarzy.html', danekontaktowe=danekontaktowe)
    else:
        msg = 'Nie zanleziono lekarzy'
        return render_template('listalekarzy.html', msg=msg)


@app.route('/chat', methods=['GET', 'POST'])
@is_logged_in('user')
@db_connection
def chat(cur):
    lekarz = {'id': 'lekarz_id', 'imie': 'lekarz_imie', 'nazwisko': 'lekarz_nazwisko'}
    pacjent = {'id': 'pacjent_id', 'imie': 'pacjent_imie', 'nazwisko': 'pacjent_nazwisko'}
    chat_type = request.args.get('chat_type')
    for key, value in lekarz.items():
        lekarz[key] = request.args.get(value)

    for key, value in pacjent.items():
        pacjent[key] = request.args.get(value)

    if request.method == 'POST':
        chat = fetch_chat(lekarz['id'], pacjent['id'])
        file = request.files['file']
        filename = secure_filename(file.filename)
        path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(path)
        path = path[8:]
        cur.execute("INSERT INTO zdjecia(ChatID, sciezka) VALUES('{}', '{}')".format(chat['id'], path))

    if chat_type == 'lekarz':
        fetched_chat = fetch_chat(lekarz['id'], pacjent['id'], True)
        pacjent['czy_lekarz'] = True
        if not fetched_chat:
            cur.execute("INSERT INTO lekarz_chat(OdbiorcaID, NadawcaID) VALUES({}, {})".format(lekarz['id'], pacjent['id']))
            fetched_chat = fetch_chat(lekarz['id'], pacjent['id'], True)
        cur.execute("SELECT * FROM lekarz_wiadomosci WHERE ChatID = {}".format(fetched_chat['id']))
        chat_history = cur.fetchall()
        cur.execute("SELECT * FROM zdjecia WHERE ChatID = {}".format(fetched_chat['id']))
        zdjecia = cur.fetchall()

    elif chat_type == 'pacjent':
        fetched_chat = fetch_chat(lekarz['id'], pacjent['id'])
        if not fetched_chat:
            cur.execute("INSERT INTO pacjent_chat(OdbiorcaID, NadawcaID) VALUES({}, {})".format(lekarz['id'], pacjent['id']))
            fetched_chat = fetch_chat(lekarz['id'], pacjent['id'])
        cur.execute("SELECT * FROM pacjent_wiadomosci WHERE ChatID = {}".format(fetched_chat['id']))
        chat_history = cur.fetchall()
        cur.execute("SELECT * FROM zdjecia WHERE ChatID = {}".format(fetched_chat['id']))
        zdjecia = cur.fetchall()

    return render_template('chat.html', lekarz=lekarz, pacjent=pacjent, chat_id=fetched_chat['id'],
                           chat_history=chat_history, zdjecia=zdjecia)


@app.route('/profillekarza')
@db_connection
def profillekarza(cur):
    chat_type = None
    id = request.args.get('lekarz_id')
    cur.execute("SELECT * FROM lekarz WHERE lekarz.id = {}".format(id))
    dane_lekarza = cur.fetchone()

    if session.get('logged_inL', False):
        chat_type = 'lekarz'
    elif session.get('logged_inP', False):
        chat_type = 'pacjent'

    lekarz = {'id': 'lekarz_id', 'imie': 'lekarz_imie', 'nazwisko': 'lekarz_nazwisko'}
    pacjent = {'id': 'pacjent_id', 'imie': 'pacjent_imie', 'nazwisko': 'pacjent_nazwisko'}
    for key, value in lekarz.items():
        lekarz[key] = request.args.get(value)

    for key, value in pacjent.items():
        pacjent[key] = request.args.get(value)

    return render_template('profillekarza.html', dane_lekarz=lekarz, dane_pacjent=pacjent, dane_lekarza=dane_lekarza,
                           chat_type=chat_type)

